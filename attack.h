/*************************
 *
 * Attack header
 *
 * BRIEF:
 *
 *  + Class for all any offensive,
 *    defensive or utility spell,
 *    attack or ability that is
 *    typically used in-battle.
 *
 * TODO:
 *
 *  + possibly move (de)buffs to a
 *    new class to keep this file
 *    cleaner
 *
 ***************************/

#ifndef __ATTACK_H__
#define __ATTACK_H__

#include <string>

enum element
{
    ELE_FIRE,
    ELE_WATER,
    ELE_WIND,
    ELE_EARTH,
    ELE_DARK,
    ELE_LIGHT,
    ELE_TIME,
    ELE_SPACE
};

enum affliction
{
    AFFLICTION_NONE,

    MAJ_DEB_POISON,
    MAJ_DEB_BURN,
    MAJ_DEB_STUN,
    MAJ_DEB_BLIND,
    MAJ_DEB_SLOW,
    MAJ_DEB_SLEEP,
    MAJ_DEB_CONFUSED,
    MAJ_DEB_ALLSTATSDOWN,

    MIN_DEB_STRDOWN,
    MIN_DEB_DEFDOWN,
    MIN_DEB_MAGDOWN,
    MIN_DEB_RESDOWN,
    MIN_DEB_DEXDOWN,

    MIN_BOON_STRUP,
    MIN_BOON_DEFUP,
    MIN_BOON_MAGUP,
    MIN_BOON_RESUP,
    MIN_BOON_DEXUP,
    
    MAJ_BOON_HEAL20,
    MAJ_BOON_HEAL45,
    MAJ_BOON_HEAL60,
    MAJ_BOON_HEALTEAM,
    MAJ_BOON_CURESTATUS,
    MAJ_BOON_REVIVE
};



class attack;

class attackCore
{
    public:
        attack* newAttack();

        unsigned getID() const;
        std:: string getName() const;
        std:: string getDescription() const;
        element getDamageType() const;
        unsigned getDamage() const;
        double getStatusChance() const;

        // mutators arguably unecessary, as attacks
        // are rather static entities

    private:
        unsigned ID;
        std:: string name;
        std:: string description;
        element damageType;
        unsigned baseDamage;
        double statusChance;
        affliction status;

};

class attack
{
    friend class attackCore;

    public:
        // runs RNG to see if debuff on opponent takes place
        // make sure to INLINE this method
        bool onDamage();

    private:
        attack(attackCore & a) : attackData_(a) {}
        attackCore& attackData_;
};

//----  attackCore methods INLINED ----//

inline attack* attackCore:: newAttack() { return new attack(*this); }

inline unsigned attackCore:: getID() const               { return ID; }
inline std:: string attackCore:: getName() const         { return name; }
inline std:: string attackCore:: getDescription()const   { return description; }
inline element attackCore:: getDamageType() const        { return damageType; }
inline unsigned attackCore:: getDamage() const           { return baseDamage; }
inline double attackCore:: getStatusChance() const       { return statusChance; }


//----  attack methods INLINED ----//

inline bool attack:: onDamage() { return true; }

#endif
