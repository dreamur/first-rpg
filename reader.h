/*************************
 *
 * Reader header
 *
 * BRIEF:
 *
 *  + Manager for locating data in CSV format.
 *    The end goal is to return the split data
 *    and let the requesting object manage the
 *    information appropriately.
 *
 * TODO:
 *
 *  + Make methods return pointers to vectors
 *  + Add asserts
 *
 ***************************/

#ifndef __READER_H__
#define __READER_H__

#include <string>
#include <vector>
#include <fstream>

using std:: string;

///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

class Reader
{
    public:
        static std:: vector<std:: string> loadData(const unsigned & ID, const std:: string & path);

    private:
        std:: vector<std:: string> split(const std:: string& s);
};


////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// Method DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


inline std:: vector<std:: string> Reader:: loadData(const unsigned & ID, const std:: string & path)
{
    std:: fstream ifstr (path, std:: ifstream:: in);

    string line;
    std:: vector<string> tmp;

    Reader r;

    while (std:: getline(ifstr, line))
    {
        tmp = r.split(line);

        if (stoi(tmp[0], nullptr, 0) == ID)
        {
            return std:: move(tmp);
        }
    }
}

inline std:: vector<std:: string> Reader:: split(const std:: string & s)
{
    std:: vector<string> vs;
    string:: size_type one, two = 0;

    one = s.find_first_not_of(',', two);
    string tempStr;

    while (one != string:: npos)
    {
        if ( (two = s.find_first_of(',', one) ) == std:: string:: npos )
        {
            tempStr = s.substr(one);
            one = two;
        }
        else
        {
            tempStr = s.substr(one, two-one);
            one = s.find_first_not_of(',', two);
        }
        vs.emplace_back(tempStr);
    }
    return std:: move(vs);
}

#endif
