/*************************
 *
 * Inventory header
 *
 * TODO:
 *
 *  + To allow 'stacking,' make the vector<item>
 *    a map<item, int> instead.
 *
 ***************************/


#ifndef __INVENTORY_H__
#define __INVENTORY_H__

#include "item.h" /* reader >> string, vector */
#include <map>
#include <algorithm>


///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


class inventory
{
    public:
        void addItem(const item it);
        void dropItem(item it);

        void sortByNameUp();
        void sortByNameDown();
        void sortByValueUp();
        void sortByValueDown();
        void sortByIDUp();
        void sortByIDDown();
        inventory();

    private:

        std:: map<item, unsigned> itemList;
        unsigned currentItems, maxItems;

        bool isFull() { return itemList.size() == maxItems; }
        bool isEmpty() { return currentItems == 0; }
};


////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// Method DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


inventory:: inventory()
{
    maxItems = 250;
    currentItems = itemList.size();
}

void inventory:: addItem(const item it)
{
    if ( isFull() )
    {
        // display error message text to screen
        return;
    }
    this->itemList[it]++;
}

void inventory:: dropItem(item it)
{
    if ( isEmpty() )
    {
        // display error message text to screen
        return;
    }
    if (this->itemList.count(it) == 1)
    {
        itemList.erase(it);
    }
}

void inventory:: sortByNameUp()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getName() < it2.getName();
    }); // end sort fn
}

void inventory:: sortByNameDown()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getName() > it2.getName();
    }); // end sort fn
}

void inventory:: sortByValueUp()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getValue() < it2.getValue();
    }); // end sort fn
}

void inventory:: sortByValueDown()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getValue() > it2.getValue();
    }); // end sort fn
}

void inventory:: sortByIDUp()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getID() < it2.getID();
    }); // end sort fn
}

void inventory:: sortByIDDown()
{
    std:: sort(itemList.begin(), itemList.end(), [](item& it1, item& it2)
    {
        return it1.getID() > it2.getID();
    }); // end sort fn
}

#endif
