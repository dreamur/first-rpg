/*************************
 *
 * Monster header
 *
 * BRIEF:
 *  + Interface for monsters
 *  + Note: the breeds will be
 *    instantiated within the zones
 *    (for the spawn lists).
 *
 * TODO:
 *
 *  + Plan for exp within breed as well
 *  + Implement attacks
 *  + Make animatible
 *
 ***************************/

#ifndef __MONSTER_H__
#define __MONSTER_H__

//#include "isAnimatible.h"
//#include "attack.h"
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <tuple>

///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// STATE ENUMS BELOW \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


// superstate enum for monster
enum monsterState
{
    // the superstate designating
    // that the monster is in a
    // battle
    MS_BATTLE,

    // the superstate designating
    // that this monster is an
    // entity in the overworld
    // that can spawn a battle
    MS_OVERWORLD
};

enum monsterBaiState
{
    // monster is dead and no longer
    // participating in battle
    MS_BaiDEAD,

    // monster is currently taking
    // its turn
    MS_BCURRENTTURN,

    // monster is not currently
    // taking its turn
    MS_BWAITING,

    // monster's health is below
    // the 15% threshold
    MS_BLOWHEALTH,

    // monster will attack the
    // actor with the lowest number
    // of HP (not percentage)
    MS_BAGGRESIVE,

    // monster's stats boosted
    // a new attack is introduced to
    // the attack rotation
    MS_BSECONDPHASE,

    // monster detects no valid target
    // and is currently roaming
    MS_BDOCILE,

    // monster detects a valid target
    // and is actively pursuing them
    MS_BHOSTILE
};

enum monsterBimgState
{
    // monster's battle idle
    // state (most common with MS_WAITING)
    MS_BIDLE,

    // monster's battle state
    // for physical attacks
    MS_BATTACKING,

    // monster's battle state for
    // casting spells
    MS_BCASTING,

    // monster's battle state
    // when taking damage
    MS_BDAMAGED,

    // monster's battle state
    // when HP <= 0
    MS_BimgDEAD,

    // monster is facing (east), (west), (north), (south)
    // in the overworld and corresponding idle is looping
    MS_ORIGHT,
    MS_OLEFT,
    MS_OBACK,
    MS_OFRONT
};

typedef std::vector<std::tuple<unsigned,unsigned,unsigned,unsigned>> VTuuuu;
typedef std::tuple<unsigned,unsigned,unsigned,unsigned> Tuuuu;

///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

class monster;

class breed
{
   public:
       monster* newMonster();

       //breed();
       breed(std:: string n, std:: string d,
             unsigned str, unsigned def,
             unsigned mag, unsigned res,
             unsigned dex, unsigned hp);

       std:: string getName() const;
       std:: string getDesc() const;
       unsigned getStr() const;
       unsigned getDef() const;
       unsigned getMag() const;
       unsigned getRes() const;
       unsigned getDex() const;
       unsigned getHp() const;
       sf:: Texture getTexture() const;
       VTuuuu getDimList() const;

       void setName(const std:: string & n);
       void setDesc(const std:: string & d);
       void setStr(const unsigned & s);
       void setDef(const unsigned & d);
       void setMag(const unsigned & m);
       void setRes(const unsigned & r);
       void setDex(const unsigned & d);
       void setHp(const unsigned & h);

       void setTexPath(const std:: string  & p);
       void setTex();

   private:
       std:: string name;
       std:: string description;

       unsigned STR, DEF, MAG, RES, DEX, HP;
       //std:: vector<attack> attackList; hidden for now

       std:: string texPath;
       sf:: Texture tex;

       VTuuuu dimensions_list;
};

class monster// : public isAnimatable , public Drawable
{
    friend class breed;

    public:
        std:: string getName() const;
        std:: string getDesc() const;
        unsigned getStr() const;
        unsigned getDef() const;
        unsigned getMag() const;
        unsigned getRes() const;
        unsigned getDex() const;
        unsigned getHp() const;
        sf:: Sprite getSprite() const;
        unsigned getRectLeft() const;
        unsigned getExpValue() const;

        // arguably the only rect member needed
        // by external sources. otherwise,
        // the entire dimension set can & will
        // be updated together
        void incrRectLeft(const int & n);
        void setRectLeft(const unsigned & n);
        void setBreed(const breed b);
        //void animate(); // tbd

    private:

        // state
        monster(breed & b);
        unsigned health_;
        breed & breedtype_;
        sf:: IntRect srcRect_;
        sf:: Sprite spriteObj;
        unsigned expValue;
};


///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


//---- monster methods --//

inline std:: string monster:: getName() const { return breedtype_.getName(); }
inline std:: string monster:: getDesc() const { return breedtype_.getDesc(); }
inline unsigned monster:: getStr() const { return breedtype_.getStr(); }
inline unsigned monster:: getDef() const { return breedtype_.getDef(); }
inline unsigned monster:: getMag() const { return breedtype_.getMag(); }
inline unsigned monster:: getRes() const { return breedtype_.getRes(); }
inline unsigned monster:: getDex() const { return breedtype_.getDex(); }
inline unsigned monster:: getHp() const { return breedtype_.getHp(); }
inline sf:: Sprite  monster:: getSprite() const { return spriteObj; }
inline unsigned monster:: getRectLeft() const { return srcRect_.left; }
inline unsigned monster:: getExpValue() const { return expValue; }

inline void monster:: incrRectLeft(const int & n) { srcRect_.left += n; }
inline void monster:: setRectLeft(const unsigned & n) { srcRect_.left = n; }
inline void monster:: setBreed(const breed b) { breedtype_ = b; }

inline monster:: monster(breed & b) : health_(b.getHp()), breedtype_(b)
{
    VTuuuu tmp = b.getDimList();
    Tuuuu ni = tmp[0];
    srcRect_.left   = std::get<0>(ni);
    srcRect_.top    = std::get<1>(ni);
    srcRect_.width  = std::get<2>(ni);
    srcRect_.height = std::get<3>(ni);

    spriteObj.setTexture(b.getTexture());
    spriteObj.setTextureRect(srcRect_);
}

//---- Breed methods --//

inline monster* breed:: newMonster() { return new monster(*this); }

inline breed:: breed(std:: string n, std:: string d,
     unsigned str, unsigned def,
     unsigned mag, unsigned res,
     unsigned dex, unsigned hp)
{
   name = n;
   description = d;
   STR = str; DEF = def; MAG = mag; RES = res; DEX = dex; HP = hp;
   dimensions_list.emplace_back(std:: make_tuple(7,81,22,60));
}

inline std:: string breed:: getName() const { return name; }
inline std:: string breed:: getDesc() const { return description; }
inline unsigned breed:: getStr() const { return STR; }
inline unsigned breed:: getDef() const { return DEF; }
inline unsigned breed:: getMag() const { return MAG; }
inline unsigned breed:: getRes() const { return RES; }
inline unsigned breed:: getDex() const { return DEX; }
inline unsigned breed:: getHp() const { return HP; }
inline sf:: Texture breed:: getTexture() const { return tex; }
inline VTuuuu breed:: getDimList() const { return dimensions_list; }

inline void breed:: setName(const std:: string & n) { name = n; }
inline void breed:: setDesc(const std:: string & d) { description = d; }
inline void breed:: setStr(const unsigned & s) { STR = s; }
inline void breed:: setDef(const unsigned & d) { DEF = d; }
inline void breed:: setMag(const unsigned & m) { MAG = m; }
inline void breed:: setRes(const unsigned & r) { RES = r; }
inline void breed:: setDex(const unsigned & d) { DEX = d; }
inline void breed:: setHp(const unsigned & h) { HP = h; }

inline void breed:: setTexPath(const std:: string & p) { texPath = p; }
inline void breed:: setTex() { tex.loadFromFile(texPath); }


#endif
