/*************************
 *
 * Cast (party) header
 *
 * TODO:
 *
 *
 ***************************/


#ifndef __CAST_H__
#define __CAST_H__

#include "actor.h" /* person >> iostream, item, reader >> string vector */
#include <algorithm> // for std:: swap

class cast
{
    public:
        void swapMembers(unsigned& id_1_, unsigned& id_2_);

    private:
        vector<actor> castList;

        void setActive(actor& a);
        void setInActive(actor& a);
        void addActor(const actor& a);
};

inline void cast:: swapMembers(unsigned& id_1_, unsigned& id_2_)
{
    unsigned in1, in2;
    unsigned index = 0; // i cheated - i'll fix it

    for (auto actr : castList)
    {
        if (actr.getID() == id_1_) { in1 = index; }
        else if (actr.getID() == id_2_) { in2 = index; }
        index++;
    }

    std:: swap(castList[in1], castList[in2]);
}

inline void cast:: setActive(actor& a) { a.makeActive(); }
inline void cast:: setInActive(actor& a) { a.makeInActive(); }
inline void cast:: addActor(const actor& a) { castList.emplace_back(a); }


#endif
