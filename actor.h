/*************************
 *
 * Actor header
 *
 * TODO:
 *
 *  + finish methods in actor class
 *  + complete protagonist class
 *
 ***************************/


#ifndef __ACTOR_H__
#define __ACTOR_H__

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
//#include "fuzzy.h"
#include "attack.h"
//#include "item.h" /* reader >> string, vector */
using std:: string;
using std:: vector;


///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


class actor
{
    public:
        //actor ();
        //void init(const string& path);

        // simply add a quantity of xp to an actor's xp pool
        void increaseEXP(const unsigned & u);

        // raise character's lvl by 1
        void increaseLevel();

        // determine xp needed for next level up
        // lvl + 300 * pow(2, lvl/7)
        void calcNewXPGoal();

        string getName() const;
        unsigned getLevel() const;
        unsigned getXP() const;
        unsigned getNextXP() const;
        unsigned getID() const;
        //unsigned getStats() { return stats; }

        void setName(const string& n);
        void setLevel(const unsigned& l);
        void setExp(const unsigned& e);
        void setNextXP(const unsigned& n);

        // toggle the active flag
        void makeActive();
        void makeInActive();


    private:
        string name;
        unsigned level;
        unsigned xp, nextXP;
        unsigned HP, STR, DEF, RES, DEX, MAG;
        unsigned ID;
        //unsigned affinity;  -- might be removed
        vector<attack> atkList;
        bool isActive;
        //weapon* weapon_;
        //armor* shield_;
};

/*class protagonist : public actor
{
    public:

        void incrWE(const int& n) { waterEarthAffin += n; }
        void incrFA(const int& n) { fireAirAffin += n; }
        void incrDL(const int& n) { darkLightAlign += n; }
        void incrST(const int& n) { spaceTimeAffin += n; }

    private:

        int waterEarthAffin, fireAirAffin, darkLightAlign, spaceTimeAffin;

        //fuzzyCore fuzzyController;


        NOTE: fuzzy logic may be removed.
        It is hidden for now.
};*/


///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


inline void actor:: increaseEXP(const unsigned & expVal) { xp += expVal; }
inline void actor:: increaseLevel() { level += 1; }

inline string actor:: getName() const { return name; }
inline unsigned actor:: getLevel() const { return level; }
inline unsigned actor:: getXP() const { return xp; }
inline unsigned actor:: getNextXP() const { return nextXP; }
inline unsigned actor:: getID() const { return ID; }

inline void actor:: setName(const string& n) { name = n; }
inline void actor:: setLevel(const unsigned& l) { level = l; }
inline void actor:: setExp(const unsigned& e) { xp = e; }
inline void actor:: setNextXP(const unsigned& n) { nextXP = n; }

inline void actor:: calcNewXPGoal()
{
    int x = round(level + 300 * pow(2, (float)level/7) );
    nextXP = x;
}

inline void actor:: makeActive() { isActive = true; }
inline void actor:: makeInActive() { isActive = false; }

#endif
