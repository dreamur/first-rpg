/*************************
 *
 * StatManager header
 *
 * BRIEF:
 *  + controller for actor levelling & experience gain
 *
 * TODO:
 *
 *  + ---
 *
 ***************************/


#ifndef __STATMGR_H__
#define __STATMGR_H__

#include "actor.h"
#include "monster.h"

class statMgr
{
    public:

        // pass the monster in,
        // store the exp value
        void encodeEXP(const monster & m);

        // pass each actor in,
        // increase their exp
        void giveBoost(actor & a);

        // to be safe,
        // zero out expValue
        // when finished
        void decodeEXP();

    private:

        // pass new lvl into
        // 2.015*0.005[(LVL+12)*2]^2
        // to get new fast
        // growing stats
        unsigned calcFastStats(const unsigned & lvl);

        // pass new lvl into
        // 2*0.005[(LVL+12)*2]^2
        // to get new normal
        // growing stats
        unsigned calcNormStats(const unsigned & lvl);

        // pass new lvl into
        // 1.9*0.005[(LVL+20s)*0.9]^2
        // to get new slow
        // growing stats
        unsigned calcSlowStats(const unsigned & lvl);

        unsigned expValue;
};

inline void statMgr:: encodeEXP(const monster & m) { expValue = m.getExpValue(); }

inline void statMgr:: giveBoost(actor & a)
{
    a.increaseEXP(expValue);

    if (a.getXP() >= a.getNextXP())
    {
        a.increaseLevel();
        //a.calcNewXPGoal();

        unsigned lvl = a.getLevel();
        unsigned fastThresh = calcFastStats(lvl);
        unsigned normThresh = calcNormStats(lvl);
        unsigned slowThresh = calcSlowStats(lvl);

        //--- make an array for stat offsets ---//
        //--- the offsets combined with thresholds ---//
        //--- give true base stats for lvl ---//
    }
}

inline void statMgr:: decodeEXP() { expValue = 0; }
inline unsigned statMgr:: calcFastStats(const unsigned& lvl) { return round( 2.015*0.005*( ((lvl+12)*2)*((lvl+12)*2) ) ); }
inline unsigned statMgr:: calcNormStats(const unsigned& lvl) { return round( 2*0.005*( ((lvl+12)*2)*((lvl+12)*2) ) ); }
inline unsigned statMgr:: calcSlowStats(const unsigned& lvl) { return round( 1.9*0.005*( ((lvl+20)*.9)*((lvl+20)*.9) ) ); }

#endif
