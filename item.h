/*************************
 *
 * Item header
 *
 * TODO:
 *
 *  + Define the Consummable class
 *  + Add asserts & exceptions
 *
 ***************************/


#ifndef __ITEM_H__
#define __ITEM_H__

#include <iostream>
#include <string>

///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// CLASS DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
//////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


//---- shouldn't need a factory or type object here ----//

class item
{
    public:
        virtual std:: string getName();
        virtual unsigned getValue() const;
        virtual unsigned getID() const;
        /*std:: string getFT() const;
        */

    protected:
        std:: string name;
        std:: string flavorText;
        unsigned baseValue;
        unsigned ID;
};

class keyItem : public item
{
    public:

    private:
};

class armor : public item
{
    public:
        unsigned getResistanceBonus() const;
        unsigned getArmorBonus() const;

        // no mutators necessary - armor can't be modified

    private:
        unsigned resistanceBonus, armorBonus;
};

class weapon : public item
{
    public:
        unsigned getMagicBonus() const;
        unsigned getStrengthBonus() const;
        unsigned getCriticalChanceBonus() const;

        // no mutators necessary - weapons can't be modified

    private:
        unsigned magicBonus, strengthBonus, criticalChanceBonus;
};

class consummable : public item
{
    friend class itemCore;

    public:

    private:
};

////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////// Method DEFINITIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////////////////////////////////////          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//---- item methods inlined ----//

    // item is virtual, but //
    // no definitions allowed //


//---- keyItem methods inlind ----//

    // none necessary ? //


//---- armor methods inlined ----//

inline unsigned armor:: getResistanceBonus() const { return resistanceBonus; }
inline unsigned armor:: getArmorBonus() const { return armorBonus; }


//---- weapon methods inlined ----//

inline unsigned weapon:: getMagicBonus() const { return magicBonus; }
inline unsigned weapon:: getStrengthBonus() const { return strengthBonus; }
inline unsigned weapon:: getCriticalChanceBonus() const { return criticalChanceBonus; }


//---- consummable methods inlined ----//

    // Still a work in progress //


#endif
