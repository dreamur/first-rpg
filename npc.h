/*************************
 *
 * NPC header
 *
 * TODO:
 *
 *  + Make inventory a vector of pointers to items
 *
 ***************************/

#ifndef __NPC_H__
#define __NPC_H__

#include "item.h" /* vector && reader.h && iostream */

class npc
{
    public:
        npc() { name = "__NULL"; r.init("CSV_npcs.csv");  item p; inventory.emplace_back( p.init(0x0000) ); }
        npc(const unsigned & id_)
        {
            r.init("CSV_npcs.csv");
            std:: vector<string> vs = r.loadData(id_);

            ID = stoi(vs[0], nullptr, 0);
            name = vs[1];

            item it;

            if (vs.size() > 2)
                for (int i = 2; i < vs.size(); i++)
                {
                    inventory.emplace_back( it.init( stoi(vs[i], nullptr, 0)) );
                }
        }

        unsigned getID() { return ID; }
        string getName() { return name; }
        /*std:: vector<item*> getInventory()
        {
            if (inventory.size() < 1) { std:: cout << "Inventory Empty"; std:: vector<item*> vi; return vi;}
        }*/
        void showInventory()
        {
            if (inventory.size() < 1) { std:: cout << "Inventory Empty"; return; }
            else
            {
                for (int i = 0; i < inventory.size(); i++)
                {
                    std:: cout << inventory[i] << '\n';
                }
            }
        }

        void setID(const unsigned & id_) { ID = id_; }
        void setName(const string & n) { name = n; }

    private:
        unsigned ID;
        string name;
        std:: vector<item> inventory;

        Reader r;
};

#endif
