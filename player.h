/*************************
 *
 * Player header
 *
 * BRIEF:
 *  + interface/ wrapper for components that player
 *    has direct access to
 *
 * TODO:
 *
 *  + Implement Stats/ Elements/ Alignment/ Fuzzy Logic
 *
 ***************************/

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "inventory.h"
#include "cast.h"

class player
{
    public:
        // getters & setters
        // init

    private:
        inventory playerInv;
        cast actorList;
        unsigned gold;

        // used to identify file name
        // for read and write
        std:: string fileName;
};

#endif
